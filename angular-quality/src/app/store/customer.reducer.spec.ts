import { SetCustomer } from './customer.actions';
import { initialState, reducer } from './customer.reducer';

describe('Customer Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('SetCustomer action', () => {
    it('should return the set customer', () => {
      const action = new SetCustomer({number: 4711, name: 'Helene Birne'});

      const result = reducer(initialState, action);

      expect(result).toEqual({number: 4711, name: 'Helene Birne'});
    });
  });
});
